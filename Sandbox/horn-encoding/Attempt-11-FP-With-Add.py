from z3 import *

# doesn't work

fp = Fixedpoint()
fp.set(engine='datalog')
# fp.set('datalog.generate_explanations', True)
s = BitVecSort(3)

edge = Function('edge', s, s, BoolSort())
path = Function('path', s, s, BoolSort())
connected = Function('connected', s, s, BoolSort())
node = Function('node', s, BoolSort())
fail_edge = Function('fail_edge', s, s, BoolSort())
not_kconnected = Function('not_kconnected', s, BoolSort())
a = Const('a',s)
b = Const('b',s)
c = Const('c',s)
k = Const('k',s)

fp.register_relation(path, edge, fail_edge, node)
fp.register_relation(connected, edge, fail_edge)
fp.register_relation(not_kconnected, connected, node)

fp.declare_var(a,b,c,k)
fp.rule(path(a,b), And(node(a),node(b), edge(a,b), Not(fail_edge(a, b))), name = 'rule_path_base_case')
fp.rule(path(a,c), And(edge(a,b), Not(fail_edge(a, b)), path(b,c)), name = 'rule_path_inductive_case')
fp.rule(connected(a, b), Or(path(a, b), path(b, a)), name = 'rule_connected')

fp.rule(not_kconnected(1), [
    node(a),
    node(b),
    Not(connected(a, b))
])

fp.rule(not_kconnected(k), [
    k > 1,
    edge(a, b),
    fail_edge(a, b),
    not_kconnected(k - 1)
])

# rule won't fire:

# fp.rule(not_kconnected(k), [
#     k > 1,
#     Implies(
#         And([
#             edge(a, b),
#             Not(fail_edge(a, b))
#         ]),
#         fail_edge(a, b)
#     ),
#     not_kconnected(k - 1)
# ])

v1 = BitVecVal(1,s)
v2 = BitVecVal(2,s)
v3 = BitVecVal(3,s)
v4 = BitVecVal(4,s)

fp.fact(node(v1))
fp.fact(node(v2))
fp.fact(node(v3))
fp.fact(node(v4))

fp.fact(edge(v1,v2), 'edge_v1_v2')
fp.fact(edge(v1,v4), 'edge_v1_v4')
fp.fact(edge(v2,v3), 'edge_v2_v3')
fp.fact(edge(v4,v3), 'edge_v4_v3')
fp.fact(edge(v3,v1), 'edge_v3_v1')

edges = [(v1, v2), (v2, v3), (v1, v4), (v4, v3), (v3, v1)]

or_exp = []
for i in range(len(edges)):
    # you can fail one edge
    vex1, vex2 = edges[i]
    and_exp = [fail_edge(vex1, vex2)]
    for j in range(len(edges)):
        if i == j:
            continue
        # and only one edge
        vex3, vex4 = edges[j]
        and_exp += [Not(fail_edge(vex3, vex4))]
    or_exp.append(And(and_exp))
fp.add(Or(or_exp))

fp.fact(fail_edge(v3, v1))

print(fp.query(not_kconnected(2)))