# Sandbox

These are some files that were used to play around with the program and coding logic. I'm just keeping them here for hindsight.

## Instructions for main.c

To compile main.c, first download Boolector, compile it and copy the entire boolector folder into this directory.

Then type

```
gcc main.c ./boolector/build/lib/libboolector.a ./boolector/deps/lingeling/liblgl.a ./boolector/deps/btor2tools/build/libbtor2parser.a -I ./boolector/src/ -I ./boolector/deps/lingeling/ -lpthread -lm
```

Now just run the resulting executable.

## Python files in horn-encoding

To run any of the Python scripts in the horn-encoding folder, just run 

```
python <filename>
```


