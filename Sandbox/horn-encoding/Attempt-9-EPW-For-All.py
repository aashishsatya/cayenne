from z3 import *

BIT_VEC_SIZE = 3

s = BitVecSort(BIT_VEC_SIZE)

edge = Function('edge', s, s, BoolSort())
path = Function('path', s, s, BoolSort())
node = Function('node', s, BoolSort())
fail_edge = Function('fail_edge', s, s, BoolSort())

v1 = BitVecVal(0,s)
v2 = BitVecVal(1,s)
v3 = BitVecVal(2,s)
# v4 = BitVecVal(3,s)

a = BitVec('a',s)
b = BitVec('b',s)
c = BitVec('c',s)

solv = Solver()

vexs = [v1, v2, v3]
edges = [
    (v1, v2),
    (v2, v1),
    (v1, v3),
    (v3, v1),
    (v2, v3),
    (v3, v2)
]

for vex in vexs:
    solv.add(node(vex))
# all the other numbers cannot form nodes
for i in range(len(vexs), 2 ** BIT_VEC_SIZE):
    solv.add(Not(node(i)))

# add the edges in
for vex1, vex2 in edges:
    solv.add(edge(vex1, vex2))

for vex1 in vexs:
    for vex2 in vexs:
        if (vex1, vex2) not in edges:
            # these edges don't exist
            solv.add(Not(edge(vex1, vex2)))
        else:
            # edge => path
            solv.add(Implies(
                And(
                    edge(vex1, vex2),
                    Not(fail_edge(vex1, vex2))
                ),
                path(vex1, vex2)
            ))

for vex1 in vexs:
    for vex2 in vexs:
        or_exp = [And(
            edge(vex1, vex2),
            Not(fail_edge(vex1, vex2))
        )]
        for vex3 in vexs:
            solv.add(Implies(
                And([
                    edge(vex1, vex3),
                    Not(fail_edge(vex1, vex3)),
                    path(vex3, vex2)
                ]),
                path(vex1, vex2)
            ))
            or_exp += [And(
                edge(vex1, vex3),
                Not(fail_edge(vex1, vex3)),
                path(vex3, vex2)
            )]
        solv.add(Implies(
            path(vex1, vex2),
            Or(or_exp)
        ))

or_exp = []
for i in range(len(edges)):
    # can you fail one edge
    vex1, vex2 = edges[i]
    and_exp = [fail_edge(vex1, vex2)]
    for j in range(len(edges)):
        if i == j:
            continue
        # and only one edge
        vex3, vex4 = edges[j]
        and_exp += [Not(fail_edge(vex3, vex4))]
    or_exp.append(And(and_exp))
solv.add(Or(or_exp))
# so that there is a pair that is not connected
solv.add(Exists([a, b], And([node(a), node(b), Not(path(a, b))])))
# solv.add(ForAll([a, b], Implies(
#     And([node(a), node(b)]),
#     path(a, b)
# )))

soln = solv.check()
if str(soln) != 'unsat':
    m = solv.model()
    print(m)
else:
    print(soln)