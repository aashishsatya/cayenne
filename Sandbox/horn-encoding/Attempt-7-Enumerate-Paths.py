from z3 import *

BIT_VEC_SIZE = 3

s = BitVecSort(BIT_VEC_SIZE)

edge = Function('edge', s, s, BoolSort())
path = Function('path', s, s, BoolSort())
node = Function('node', s, BoolSort())

v1 = BitVecVal(0,s)
v2 = BitVecVal(1,s)
v3 = BitVecVal(2,s)
v4 = BitVecVal(3,s)

a = BitVec('a',s)
b = BitVec('b',s)
c = BitVec('c',s)

solv = Solver()

vexs = [v1, v2, v3, v4]
edges = [(v1, v2), (v2, v3), (v1, v4), (v4, v3), (v3, v1)]

for vex in vexs:
    solv.add(node(vex))
# all the other numbers cannot form nodes
for i in range(len(vexs), 2 ** BIT_VEC_SIZE):
    solv.add(Not(node(i)))

# add the edges in
for vex1, vex2 in edges:
    solv.add(edge(vex1, vex2))

for vex1 in vexs:
    for vex2 in vexs:
        if (vex1, vex2) not in edges:
            solv.add(Not(edge(vex1, vex2)))
        else:
            solv.add(Implies(
                edge(vex1, vex2),
                path(vex1, vex2)
            ))

for vex1 in vexs:
    for vex2 in vexs:
        or_exp = [edge(vex1, vex2)]
        for vex3 in vexs:
            solv.add(Implies(
                And([edge(vex1, vex3), path(vex3, vex2)]),
                path(vex1, vex2)
            ))
            or_exp += [And(
                edge(vex1, vex3),
                path(vex3, vex2)
            )]
        solv.add(Implies(
            path(vex1, vex2),
            Or(or_exp)
        ))

solv.add(Not(Exists([a, b], And([node(a), node(b), Not(path(a, b))]))))
soln = solv.check()
# print(soln)
if str(soln) != 'unsat':
    m = solv.model()
    print(m.evaluate(path(v4, v2)))
    # print(m)
else:
    print(soln)