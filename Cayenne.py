from z3 import *
import itertools
import sys
import time

start = time.time()

# parse the input

ip = sys.stdin.readlines()
M, N, Q, k, slimit = map(int, ip[0].split(' '))
nodes = range(N)

BIT_VEC_SIZE = math.ceil(math.log2(max(M, N, Q)))
s = BitVecSort(BIT_VEC_SIZE)

a = BitVec('a',s)
b = BitVec('b',s)

mol_on_node_list = []
active_mol_on_node_list = []
inactive_mol_on_node_list = []
active_mol_on_edge_list = []
inactive_mol_on_edge_list = []
mol_on_qth_edge_list = []


qth_edge_list = []
absent_qth_edge = []  # this plays the role of AbsentEdges in the paper

solv = Solver()
node = Function('node', s, BoolSort())
mol_on_node = Function('mol_on_node', s, s, BoolSort())
qth_edge = Function('qth_edge', s, s, s, BoolSort())
mol_on_qth_edge = Function('mol_on_qth_edge', s, s, s, s, BoolSort())
pairing = Function('pairing', s, s, BoolSort())
node_activity = Function('node_activity', s, s, BoolSort())
edge_activity = Function('edge_activity', s, s, s, s, BoolSort())
m_paths = Function('m_paths', s, s, s, s, BoolSort())

# dicts for each set of failed edges
fail_edge_fns = {}
path_fns = {}

for i in range(1, len(ip), 2):
    ip_type = ip[i].strip()
    if ip_type == 'n':
        # information regarding nodes
        node_id, mols_str = ip[i + 1].split(',')
        node_id = int(node_id)
        mols_str = mols_str.strip().split(' ')
        for mol in mols_str:
            if '+' in mol:
                solv.add(node_activity(node_id, int(mol[:-1])))
                active_mol_on_node_list.append((node_id, int(mol[:-1])))
                mol_on_node_list.append((node_id, int(mol[:-1])))
            elif '-' in mol:
                solv.add(Not(node_activity(node_id, int(mol[:-1]))))
                inactive_mol_on_node_list.append((node_id, int(mol[:-1])))
                mol_on_node_list.append((node_id, int(mol[:-1])))
            else:
                # vanilla number
                solv.add(mol_on_node(node_id, int(mol)))
                mol_on_node_list.append((node_id, int(mol)))
    elif ip_type == 'e':
        # information regarding edge and edge labels
        edge_str, mols_str = ip[i + 1].split(',')
        edge_i, edge_j, edge_q = map(int, edge_str.split(' '))
        qth_edge_list.append((edge_i, edge_j, edge_q))

        mols_str = mols_str.strip().split(' ')
        for mol in mols_str:
            if '+' in mol:
                solv.add(edge_activity(edge_i, edge_j, edge_q, int(mol[:-1])))
                active_mol_on_edge_list.append((edge_i, edge_j, edge_q, int(mol[:-1])))
                mol_on_qth_edge_list.append((edge_i, edge_j, edge_q, int(mol[:-1])))
            elif '-' in mol:
                solv.add(Not(edge_activity(edge_i, edge_j, edge_q, int(mol[:-1]))))
                inactive_mol_on_edge_list.append((edge_i, edge_j, edge_q, int(mol[:-1])))
                mol_on_qth_edge_list.append((edge_i, edge_j, edge_q, int(mol[:-1])))    # molecule is present just not active
            else:
                # vanilla number
                solv.add(mol_on_qth_edge(edge_i, edge_j, edge_q, int(mol)))
                mol_on_qth_edge_list.append((edge_i, edge_j, edge_q, int(mol)))

# do not add new molecules on nodes

for i in range(N):
    for m in range(M):
        if (i, m) not in mol_on_node_list:
            solv.add(Not(mol_on_node(i, m)))

# molecules should not be added on existing edges

for i in range(N):
    for j in range(N):
        for q in range(Q):
            if (i, j, q) in qth_edge_list:
                for m in range(M):
                    if (i, j, q, m) not in mol_on_qth_edge_list:
                        solv.add(Not(mol_on_qth_edge(i, j, q, m)))

# activation functions of existing edges must not be messed with

for i in range(N):
    for j in range(N):
        for q in range(Q):
            if (i, j, q) in qth_edge_list:
                for m in range(M):
                    if (i, j, q, m) not in active_mol_on_edge_list and (i, j, q, m) not in inactive_mol_on_edge_list:
                        solv.add(Not(mol_on_qth_edge(i, j, q, m)))

for i in range(N):
    for j in range(N):
        if i == j:
            continue # no self loops, these can't be enabled
        for q in range(Q):
            if (i, j, q) not in qth_edge_list:
                # solv.add(Not(qth_edge(i, j, q)))
                absent_qth_edge.append((i, j, q))

# just a sanity check to ensure that there are no self-loops
for i in range(N):
    for q in range(Q):
        solv.add(Not(qth_edge(i, i, q)))

for n1 in nodes:
    solv.add(node(n1))
for i in range(len(nodes), 2 ** BIT_VEC_SIZE):
    # these ain't nodes
    solv.add(Not(node(i)))

# encode the pairing constraints
for i in range(M):
    for j in range(M):
        if (i < M//2 and j < M//2) or (i >= M//2 and j >= M//2):
            solv.add(Not(pairing(i, j)))

# for the PLOS-1 example

# encode the EdgeC constraint

# first part, m on qth edge => qth edge exists

for i in range(N):
    for j in range(N):
        for q in range(Q):

            or_exp = []
            for m in range(M):
                or_exp.append(mol_on_qth_edge(i, j, q, m))
                # second part, m on qth edge => m is on n_i or n_m
                solv.add(Implies(
                    mol_on_qth_edge(i, j, q, m),
                    And(
                        mol_on_node(i, m),
                        mol_on_node(j, m)
                    )
                ))

            solv.add(
                Implies(
                    Or(or_exp),
                    qth_edge(i, j, q)
                )
            )

# encode the ActivityC constraint

# first part

# m on qth edge active => m must be on qth edge

for i in range(N):
    for j in range(N):
        for q in range(Q):
            for m in range(M):
                solv.add(Implies(
                    edge_activity(i, j, q, m),
                    mol_on_qth_edge(i, j, q, m)
                ))

# m on node i active => m must be on node i

for i in range(N):
    for m in range(M):
        solv.add(Implies(
            node_activity(i, m),
            mol_on_node(i, m)
        ))

# encode the V7 constraint of Thattai paper

for i in range(N):
    for j in range(N):
        for q in range(Q):
            or_exp = []
            for m1 in range(M):
                for m2 in range(M):
                    or_exp.append(
                        And([
                            edge_activity(i, j, q, m1),
                            node_activity(j, m2),
                            pairing(m1, m2)
                        ])
                    )
            solv.add(Implies(
                qth_edge(i, j, q),
                Or(or_exp)
            ))

# dropping F2/V8 constraints

# encode m_paths(r) property

# edge => path of length 1

for i in range(N):
    for j in range(N):
        if i == j:
            continue
        for q in range(Q):
            for m in range(M):
                solv.add(Implies(
                    mol_on_qth_edge(i, j, q, m),
                    m_paths(i, j, m, 1)
                ))
        for m in range(M):
            or_exp = []
            for q in range(Q):
                or_exp.append(mol_on_qth_edge(i, j, q, m))
            solv.add(Implies(
                m_paths(i, j, m, 1),
                Or(or_exp)
            ))

for vex1 in range(N):
    for vex2 in range(N):
        if vex1 == vex2:
            continue
        for m in range(M):
            for p in range(2, N):
                or_exp = []
                for vex3 in range(N):
                    if vex3 == vex1 or vex3 == vex2:
                        continue
                    # there's an m-path between vex1 and vex2 if there is an edge between
                    # vex1 and vex3 and path between vex3 and vex2
                    solv.add(Implies(
                        And(
                            m_paths(vex1, vex3, m, 1),
                            m_paths(vex3, vex2, m, p - 1)
                        ),
                        m_paths(vex1, vex2, m, p)
                    ))
                    # if there is a path of length p - 1 there is a path of length p
                    solv.add(Implies(
                        m_paths(vex1, vex2, m, p - 1),
                        m_paths(vex1, vex2, m, p),
                    ))
                    or_exp.append(And(
                        m_paths(vex1, vex3, m, 1),
                        m_paths(vex3, vex2, m, p - 1)
                    ))
                or_exp.append(m_paths(vex1, vex2, m, p - 1))
                solv.add(Implies(
                    m_paths(vex1, vex2, m, p),
                    Or(or_exp)
                ))

# add the condition that only slimit new edges can be added

or_exp = []
for edge_combinations in itertools.combinations(absent_qth_edge, slimit):
    and_exp = []
    for i in range(len(edge_combinations)):
        edge_i, edge_j, edge_q = edge_combinations[i]
        and_exp = [qth_edge(edge_i, edge_j, edge_q)]
    for j in range(len(absent_qth_edge)):
        if absent_qth_edge[j] in edge_combinations:
            continue
        # this edge can be added
        edge2_i, edge2_j, edge2_q = absent_qth_edge[j]
        and_exp.append(Not(qth_edge(edge2_i, edge2_j, edge2_q)))
    or_exp.append(And(and_exp))
if len(or_exp) > 0:
    solv.add(Or(or_exp))

# Loop(r)

for i in range(N):
    for j in range(N):
        if i == j:
            continue
        for m in range(M):
            or_exp = []
            for q in range(Q):
                or_exp.append(mol_on_qth_edge(i, j, q, m))
            solv.add(Implies(
                Or(or_exp),
                m_paths(j, i, m, N - 1)
            ))

all_possible_edges = []
for i in range(N):
    for j in range(N):
        if i == j:
            continue
        for q in range(Q):
            all_possible_edges.append((i, j, q))

for edge_combination in itertools.combinations(all_possible_edges, k - 1):

    fail_edge_list_str = []
    for edge in edge_combination:
        fail_edge_list_str.append(''.join(map(str, edge)))

    edges_str = '_'.join(fail_edge_list_str)
    path_fn_name = 'path_' + edges_str
    path_fn = Function(path_fn_name, s, s, s, BoolSort())
    fail_edge_fn = Function('fail_edge_' + edges_str, s, s, s, BoolSort())
    path_fns[edge_combination] = path_fn
    fail_edge_fns[edge_combination] = fail_edge_fn

    # the edges in edge_combination have failed

    for edge in edge_combination:
        solv.add(fail_edge_fn(edge[0], edge[1], edge[2]))
    
    for edge_i in range(N):
        for edge_j in range(N):
            or_exp = []
            for edge_q in range(Q):
                # edge => path
                solv.add(Implies(
                    And(
                        qth_edge(edge_i, edge_j, edge_q),
                        Not(fail_edge_fn(edge_i, edge_j, edge_q))
                    ),
                    path_fn(edge_i, edge_j, 1)
                ))
                or_exp.append(And(
                    qth_edge(edge_i, edge_j, edge_q),
                    Not(fail_edge_fn(edge_i, edge_j, edge_q))
                ))
            solv.add(Implies(
                path_fn(edge_i, edge_j, 1),
                Or(or_exp)
            ))

    for node1 in nodes:
        for node2 in nodes:
            if node1 == node2:
                continue
            for p in range(2, N):
                or_exp = []
                for node3 in range(N):
                    if node3 == node1 or node3 == node2:
                        continue
                    solv.add(Implies(
                        And(
                            path_fn(node1, node3, 1),
                            path_fn(node3, node2, p - 1)
                        ),
                        path_fn(node1, node2, p)
                    ))
                    or_exp.append(And(
                        path_fn(node1, node3, 1),
                        path_fn(node3, node2, p - 1),
                    ))
                solv.add(Implies(
                    path_fn(node1, node2, p - 1),
                    path_fn(node1, node2, p)
                ))
                or_exp.append(path_fn(node1, node2, p - 1))
                solv.add(Implies(
                    path_fn(node1, node2, p),
                    Or(or_exp)
                ))

    # for all edges, there has to be a path from one to the other
    solv.add(ForAll(
        [a, b],
        Implies(
            And(node(a), node(b), a != b),
            Or(
                path_fn(a, b, N - 1),
                path_fn(b, a, N - 1)
            )
        )
    ))

    # for node1 in nodes:
    #     for node2 in nodes:
    #         if node1 == node2:
    #             continue
    #         solv.add(Or(
    #             path_fn(node1, node2, N - 1),
    #             path_fn(node2, node1, N - 1)
    #         ))

res = solv.check()
if str(res) != 'unsat':
    soln = solv.model()
    # for sol in soln:
    #     print(sol, ':')
    #     print(soln[sol])
    print('Newly added edges:')
    for edge_i, edge_j, edge_q in absent_qth_edge:
        print('Added', edge_i, edge_j, edge_q, '?:', soln.evaluate(qth_edge(edge_i, edge_j, edge_q)))
else:
    print(res)

stop = time.time()
print('Time taken =', stop - start)