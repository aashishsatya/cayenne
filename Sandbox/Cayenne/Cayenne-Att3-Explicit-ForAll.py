from z3 import *
import itertools
import sys

# parse the input

ip = sys.stdin.readlines()
M, N, Q = map(int, ip[0].split(' '))
slimit = 1
nodes = range(N)

BIT_VEC_SIZE = math.ceil(math.log2(max(M, N, Q)))
s = BitVecSort(BIT_VEC_SIZE)

a = BitVec('a',s)
b = BitVec('b',s)

mol_on_node_list = []
qth_edge_list = []
not_qth_edge_list = []  # this plays the role of AbsentEdges in the paper
mol_on_qth_edge_list = []

solv = Solver()
node = Function('node', s, BoolSort())
mol_on_node = Function('mol_on_node', s, s, BoolSort())
qth_edge = Function('qth_edge', s, s, s, BoolSort())
mol_on_qth_edge = Function('mol_on_qth_edge', s, s, s, s, BoolSort())
pairing = Function('pairing', s, s, BoolSort())
node_activity = Function('node_activity', s, s, BoolSort())
edge_activity = Function('edge_activity', s, s, s, s, BoolSort())
m_paths = Function('m_paths', s, s, s, BoolSort())

# dicts for each set of failed edges
fail_edge_fns = {}
path_fns = {}

for i in range(1, len(ip), 2):
    ip_type = ip[i].strip()
    if ip_type == 'n':
        # information regarding nodes
        node_id, mols_str = ip[i + 1].split(',')
        node_id = int(node_id)
        mols = map(int, mols_str.strip().split(' '))
        for mol in mols:
            mol_on_node_list.append((node_id, mol))
            solv.add(mol_on_node(node_id, mol))
    elif ip_type == 'e':
        # information regarding edge and edge labels
        edge_str, mols_str = ip[i + 1].split(',')
        edge_i, edge_j, edge_q = map(int, edge_str.split(' '))
        qth_edge_list.append((edge_i, edge_j, edge_q))
        mols = map(int, mols_str.strip().split(' '))
        for mol in mols:
            mol_on_qth_edge_list.append((edge_i, edge_j, edge_q, mol))
            solv.add(mol_on_qth_edge(edge_i, edge_j, edge_q, mol))

for i in range(N):
    for j in range(N):
        if i == j:
            continue # no self loops, these can't be enabled
        for q in range(Q):
            if (i, j, q) not in qth_edge_list:
                # solv.add(Not(qth_edge(i, j, q)))
                not_qth_edge_list.append((i, j, q))

# just a sanity check to ensure that there are no self-loops
for i in range(N):
    for q in range(Q):
        solv.add(Not(qth_edge(i, i, q)))

for n1 in nodes:
    solv.add(node(n1))
for i in range(len(nodes), 2 ** BIT_VEC_SIZE):
    # these ain't nodes
    solv.add(Not(node(i)))

# encode the EdgeC constraint

# first part, m on qth edge => qth edge exists

for i in range(N):
    for j in range(N):
        for q in range(Q):

            or_exp = []
            for m in range(M):
                or_exp.append(mol_on_qth_edge(i, j, q, m))
                # second part, m on qth edge => m is on n_i or n_m
                solv.add(Implies(
                    mol_on_qth_edge(i, j, q, m),
                    And(
                        mol_on_node(i, m),
                        mol_on_node(j, m)
                    )
                ))

            solv.add(
                Implies(
                    Or(or_exp),
                    qth_edge(i, j, q)
                )
            )

# encode the ActivityC constraint

# first part

# m on qth edge active => m must be on qth edge

for i in range(N):
    for j in range(N):
        for q in range(Q):
            for m in range(M):
                solv.add(Implies(
                    edge_activity(i, j, q, m),
                    mol_on_qth_edge(i, j, q, m)
                ))

# m on node i active => m must be on node i

for i in range(N):
    for m in range(M):
        solv.add(Implies(
            node_activity(i, m),
            mol_on_node(i, m)
        ))

# encode the V7 constraint of Thattai paper

for i in range(N):
    for j in range(N):
        for q in range(Q):
            or_exp = []
            for m1 in range(M):
                for m2 in range(M):
                    or_exp.append(
                        And([
                            edge_activity(i, j, q, m1),
                            node_activity(j, m2),
                            pairing(m1, m2)
                        ])
                    )
            solv.add(Implies(
                qth_edge(i, j, q),
                Or(or_exp)
            ))

# dropping F2/V8 constraints

# encode m_paths(r) property

# we set p = |nodes| by default, as that is what is used

# edge => path

for i in range(N):
    for j in range(N):
        for q in range(Q):
            for m in range(M):
                solv.add(Implies(
                    mol_on_qth_edge(i, j, q, m),
                    m_paths(i, j, m)
                ))

for vex1 in range(N):
    for vex2 in range(N):
        for m in range(M):
            path_implies_or_exp = []
            for q in range(Q):
                path_implies_or_exp.append(mol_on_qth_edge(vex1, vex2, q, m))
            
            for vex3 in range(N):
                # there's an m-path between vex1 and vex2 if there is an edge between
                # vex1 and vex3 and path between vex3 and vex2
                or_exp = []
                for q in range(Q):
                    or_exp.append(mol_on_qth_edge(vex1, vex3, q, m))
                solv.add(Implies(
                    And(
                        Or(or_exp),
                        m_paths(vex3, vex2, m)
                    ),
                    m_paths(vex1, vex2, m)
                ))
                path_implies_or_exp.append(And(
                    Or(or_exp),
                    m_paths(vex3, vex2, m)
                ))
            solv.add(Implies(
                m_paths(vex1, vex2, m),
                Or(path_implies_or_exp)
            ))

# Loop(r)

for i in range(N):
    for j in range(N):
        for m in range(M):
            or_exp = []
            for q in range(Q):
                or_exp.append(mol_on_qth_edge(i, j, q, m))
            solv.add(Implies(
                Or(or_exp),
                m_paths(j, i, m)
            ))


# add the condition that only one new edge can be added

or_exp = []
for i in range(len(not_qth_edge_list)):
    edge_i, edge_j, edge_q = not_qth_edge_list[i]
    and_exp = [qth_edge(edge_i, edge_j, edge_q)]
    for j in range(len(not_qth_edge_list)):
        if i == j:
            continue
        # only one new edge should be added
        edge2_i, edge2_j, edge2_q = not_qth_edge_list[j]
        and_exp.append(Not(qth_edge(edge2_i, edge2_j, edge2_q)))
    or_exp.append(And(and_exp))
solv.add(Or(or_exp))


for edge1, edge2 in itertools.combinations(qth_edge_list, 2):
    print('Failing', edge1, 'and', edge2, '...')
    edges_str = ''.join(map(str, edge1)) + '_' + ''.join(map(str, edge2))
    path_fn_name = 'path_' + edges_str
    path_fn = Function(path_fn_name, s, s, BoolSort())
    fail_edge_fn = Function('fail_edge_' + edges_str, s, s, s, BoolSort())
    path_fns[edge1, edge2] = path_fn
    fail_edge_fns[edge1, edge2] = fail_edge_fn

    # edges edge1 and edge2 have failed

    solv.add(fail_edge_fn(edge1[0], edge1[1], edge1[2]))
    solv.add(fail_edge_fn(edge2[0], edge2[1], edge2[2]))

    for edge_i, edge_j, edge_q in qth_edge_list:
        # edge => path
        solv.add(Implies(
            And(
                qth_edge(edge_i, edge_j, edge_q),
                Not(fail_edge_fn(edge_i, edge_j, edge_q))
            ),
            path_fn(edge_i, edge_j)
        ))

    for node1 in nodes:
        for node2 in nodes:
            # there is a path between node1 and node2 if there is some q s.t. e(node1, node2, q) holds and it hasn't failed
            path_implies_or_exp = []
            for q in range(Q):
                path_implies_or_exp.append(And(
                    qth_edge(node1, node2, q),
                    Not(fail_edge_fn(node1, node2, q))
                ))
            # or if there is an edge from node1 to node3 and a path from node3 to node2
            for node3 in nodes:
                or_exp = []
                for q in range(Q):
                    or_exp.append(And(
                        qth_edge(node1, node3, q),
                        Not(fail_edge_fn(node1, node3, q))
                    ))
                solv.add(Implies(
                    And(
                        Or(or_exp),
                        path_fn(node3, node2)
                    ),
                    path_fn(node1, node2)
                ))
                path_implies_or_exp.append(And(
                    Or(or_exp),
                    path_fn(node3, node2)
                ))
            solv.add(Implies(
                path_fn(node1, node2),
                Or(path_implies_or_exp)
            ))

    # for all edges, there has to be a path from one to the other
    # solv.add(ForAll(
    #     [a, b],
    #     Implies(
    #         And(node(a), node(b), a != b),
    #         Or(
    #             path_fn(a, b),
    #             path_fn(b, a)
    #         )
    #     )
    # ))

    for node1 in nodes:
        for node2 in nodes:
            if node1 == node2:
                continue
            solv.add(Or(
                path_fn(node1, node2),
                path_fn(node2, node1)
            ))

res = solv.check()
if str(res) != 'unsat':
    soln = solv.model()
    for sol in soln:
        print(sol, ':')
        print(soln[sol])
    print('Newly added edges:')
    for edge_i, edge_j, edge_q in not_qth_edge_list:
        print('Added', edge_i, edge_j, edge_q, '?:', soln.evaluate(qth_edge(edge_i, edge_j, edge_q)))
else:
    print(res)
    print('unsat core:', solv.unsat_core())