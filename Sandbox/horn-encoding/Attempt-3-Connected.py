from z3 import *

fp = Fixedpoint()
fp.set(engine='datalog')
# fp.set('datalog.generate_explanations', True)
s = BitVecSort(3)

edge = Function('edge', s, s, BoolSort())
path = Function('path', s, s, BoolSort())
connected = Function('connected', s, s, BoolSort())
node = Function('node', s, BoolSort())
fail_edge = Function('fail_edge', s, s, BoolSort())
a = Const('a',s)
b = Const('b',s)
c = Const('c',s)

fp.register_relation(path, edge, fail_edge, node)
fp.register_relation(connected, edge, fail_edge)

fp.declare_var(a,b,c)
fp.rule(path(a,b), And(node(a), node(b), edge(a,b), Not(fail_edge(a, b))), name = 'rule_path_base_case')
fp.rule(path(a,c), And(edge(a,b), Not(fail_edge(a, b)), path(b,c)), name = 'rule_path_inductive_case')
fp.rule(connected(a, b), Or(path(a, b), path(b, a)), name = 'rule_connected')

v1 = BitVecVal(1,s)
v2 = BitVecVal(2,s)
v3 = BitVecVal(3,s)
v4 = BitVecVal(4,s)

fp.fact(node(v1))
fp.fact(node(v2))
fp.fact(node(v3))
fp.fact(node(v4))

fp.fact(edge(v1,v2), 'edge_v1_v2')
fp.fact(edge(v1,v4), 'edge_v1_v4')
fp.fact(edge(v2,v3), 'edge_v2_v3')
fp.fact(edge(v4,v3), 'edge_v4_v3')

fp.fact(edge(v3,v2), 'edge_v3_v2')
fp.fact(edge(v3,v4), 'edge_v3_v4')
fp.fact(edge(v2,v1), 'edge_v2_v1')
fp.fact(edge(v4,v1), 'edge_v4_v1')

fp.fact(fail_edge(v3, v2))
fp.fact(fail_edge(v3, v4))

print(fp.query(connected(v3, v1)))