# Cayenne 

Cayenne is a program for vesiCle trAnsport sYstEm syNthesis usiNg smt solvErs. We're essentially attempting an implementation of a subset of [this](https://www.researchgate.net/publication/327193710_Synthesis_for_Vesicle_Traffic_Systems_16th_International_Conference_CMSB_2018_Brno_Czech_Republic_September_12-14_2018_Proceedings) paper using [Z3](https://github.com/Z3Prover/z3). 

# Input format

The file example.vts in the examples folder represents a sample VTS that can be fed into the program as input. The first line represents M, N, Q, k and slimit (the number of molecules, nodes, maximum number of edges between two nodes, k and the maximum edges you can add respectively).

# Running the program

To run the program, type in
```
python3 Cayenne.py < ./examples/<file_name>
```
