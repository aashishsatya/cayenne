from z3 import *

BIT_VEC_SIZE = 3

s = BitVecSort(BIT_VEC_SIZE)

edge = Function('edge', s, s, BoolSort())
path = Function('path', s, s, BoolSort())
node = Function('node', s, BoolSort())
fail_edge = Function('fail_edge', s, s, BoolSort())

v0 = BitVecVal(0,s)
v1 = BitVecVal(1,s)
v2 = BitVecVal(2,s)
v3 = BitVecVal(3,s)

a = BitVec('a',s)
b = BitVec('b',s)
c = BitVec('c',s)
d = BitVec('d',s)

solv = Solver()

vexs = [v0, v1, v2, v3]
edges = [(v0, v1), (v1, v2), (v0, v3), (v3, v2), (v2, v0), (v1, v3)]

for vex in vexs:
    solv.add(node(vex))
# all the other numbers cannot form nodes
for i in range(len(vexs), 2 ** BIT_VEC_SIZE):
    solv.add(Not(node(i)))

# add the edges in
for vex1, vex2 in edges:
    solv.add(edge(vex1, vex2))

for vex1 in vexs:
    for vex2 in vexs:
        if (vex1, vex2) not in edges:
            # these edges don't exist
            print('Adding that edge(', vex1, ',', vex2, ') does not exist...')
            solv.add(Not(edge(vex1, vex2)))
            solv.add(Not(fail_edge(vex1, vex2)))
        else:
            # edge => path
            solv.add(Implies(
                And(
                    edge(vex1, vex2),
                    Not(fail_edge(vex1, vex2))
                ),
                path(vex1, vex2)
            ))

for vex1, vex2 in edges:
    # for an edge to fail it has to exist
    solv.add(Implies(
        fail_edge(vex1, vex2),
        edge(vex1, vex2)
        )
    )

for vex1 in vexs:
    for vex2 in vexs:
        or_exp = [And(
            edge(vex1, vex2),
            Not(fail_edge(vex1, vex2))
        )]
        for vex3 in vexs:
            solv.add(Implies(
                And([
                    edge(vex1, vex3),
                    Not(fail_edge(vex1, vex3)),
                    path(vex3, vex2)
                ]),
                path(vex1, vex2)
            ))
            or_exp += [And(
                edge(vex1, vex3),
                Not(fail_edge(vex1, vex3)),
                path(vex3, vex2)
            )]
        solv.add(Implies(
            path(vex1, vex2),
            Or(or_exp)
        ))

# does there exists nodes a, b
# such that if those nodes fail
# then there exists nodes c, d
# such that they are disconnected
# solv.add(
#     Exists(
#         [a,b],
#         And(
#             fail_edge(a, b),
#             Exists(
#                 [c, d],
#                 And([
#                     node(c),
#                     node(d),
#                     Not(path(c, d))
#                 ])
#             )
#         )
#     )
# )

# does there exists nodes a, b and nodes c, d such that
# such that if edge (a, b) fails
# then c, d is not connected

or_exp = []
for i in range(len(edges)):
    # you can fail one edge
    vex1, vex2 = edges[i]
    and_exp = [fail_edge(vex1, vex2)]
    for j in range(len(edges)):
        if i == j:
            continue
        # and only one edge
        vex3, vex4 = edges[j]
        and_exp += [Not(fail_edge(vex3, vex4))]
    or_exp.append(And(and_exp))
solv.add(Or(or_exp))

# solv.add(
#     Exists(
#         [a,b,c,d],
#         And([
#             edge(a, b),
#             fail_edge(a, b),
#             node(c),
#             node(d),
#             Not(path(c, d))
#         ])
#     ) 
# )

# the not of the above thing:

solv.add(
    Not(
        Exists(
        [a,b,c,d],
        And([
            edge(a, b),
            fail_edge(a, b),
            node(c),
            node(d),
            Not(path(c, d))
        ])
    ))    
)


# solv.add(
#     Exists(
#         [a, b], 
#         And([
#             node(a), 
#             node(b), 
#             Not(path(a, b))
#         ])
#     )
# )

# print('Current set of rules:')
# print(solv)

solv.add(Not(edge(v3, v0)))
soln = solv.check()
if str(soln) != 'unsat':
    m = solv.model()
    for vex1, vex2 in edges:
        print('Edge (', vex1, ',', vex2, '):', m.evaluate(fail_edge(vex1, vex2)))
    print('One edge has failed?:', m.evaluate(Or(or_exp)))
    print('Edge (v3, v0) has failed?:', m.evaluate(fail_edge(v3, v0)))
    print('Edge (v3, v0) exists?:', m.evaluate(edge(v3, v0)))
    print('Not(Edge (v3, v0))?:', m.evaluate(Not(edge(v3, v0))))
    print('fail_edge(v3, v0) => edge(v3, v0)?:', m.evaluate(Implies(fail_edge(v3, v0), edge(v3, v0))))
    # print('Model:')
    # print(m)
else:
    print(soln)