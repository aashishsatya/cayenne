from z3 import *
import itertools

BIT_VEC_SIZE = 3

s = BitVecSort(BIT_VEC_SIZE)

edge = Function('edge', s, s, BoolSort())
node = Function('node', s, BoolSort())
# path = Function('path', s, s, BoolSort())
# fail_edge = Function('fail_edge', s, s, BoolSort())
fail_edge = {}
path = {}

v1 = BitVecVal(0,s)
v2 = BitVecVal(1,s)
v3 = BitVecVal(2,s)
v4 = BitVecVal(3,s)

a = BitVec('a',s)
b = BitVec('b',s)
c = BitVec('c',s)

solv = Solver()

vexs = [v1, v2, v3]
edges = [
    (v1, v2),
    (v2, v1),
    (v1, v3),
    (v3, v1),
    (v2, v3),
    (v3, v2)
]

for vex in vexs:
    solv.add(node(vex))
# all the other numbers cannot form nodes
for i in range(len(vexs), 2 ** BIT_VEC_SIZE):
    solv.add(Not(node(i)))

# add the edges in
for vex1, vex2 in edges:
    solv.add(edge(vex1, vex2))

for vex1 in vexs:
    for vex2 in vexs:
        if (vex1, vex2) not in edges:
            # these edges don't exist
            solv.add(Not(edge(vex1, vex2)))

# TODO: generalize to k

for edge1, edge2 in itertools.combinations(edges, 2):

    print('Dropping edges', edge1, edge2, '...')

    # for each combination, there is a path function and a fail_edge function

    path_fn = Function('path_' + str(edge1) + '_' + str(edge2), s, s, BoolSort())
    fail_edge_fn = Function('fail_edge_' + str(edge1) + '_' + str(edge2), s, s, BoolSort())
    path[edge1, edge2] = path_fn
    fail_edge[edge1, edge2] = fail_edge_fn

    # edges edge1 and edge2 have failed

    solv.add(fail_edge_fn(edge1[0], edge1[1]))
    solv.add(fail_edge_fn(edge2[0], edge2[1]))

    for vex1, vex2 in edges:
            # edge => path
            solv.add(Implies(
                And(
                    edge(vex1, vex2),
                    Not(fail_edge_fn(vex1, vex2))
                ),
                path_fn(vex1, vex2)
            ))

    for vex3 in vexs:
        for vex4 in vexs:
            # there is a path between vex3 and vex4 if there is an edge between them and it hasn't failed
            or_exp = [And(
                edge(vex3, vex4),
                Not(fail_edge_fn(vex3, vex4))
            )]
            # or if there is an edge from vex3 to vex5 and a path from vex5 to vex4
            for vex5 in vexs:
                solv.add(Implies(
                    And([
                        edge(vex3, vex5),
                        Not(fail_edge_fn(vex3, vex5)),
                        path_fn(vex5, vex4)
                    ]),
                    path_fn(vex3, vex4)
                ))
                or_exp += [And(
                    edge(vex3, vex5),
                    Not(fail_edge_fn(vex3, vex5)),
                    path_fn(vex5, vex4)
                )]
            solv.add(Implies(
                path_fn(vex3, vex4),
                Or(or_exp)
            ))

    # now for all edges, there has to be a path from one or the other
    solv.add(ForAll(
        [a, b],
        Implies(
            And(node(a), node(b), a != b),
            Or(
                path_fn(a, b),
                path_fn(b, a)
            )
        )
    ))

    soln = solv.check()
    if str(soln) == 'unsat':
        print('Problem has become unsat.')
        break

soln = solv.check()
if str(soln) != 'unsat':
    print('Model:')
    m = solv.model()
    print(m)
else:
    print(soln)
    print('unsat core:', solv.unsat_core())

# print(solv.to_smt2())