from z3 import *

s = BitVecSort(3)

edge = Function('edge', s, s, BoolSort())
path = Function('path', s, s, BoolSort())
node = Function('node', s, BoolSort())

v1 = BitVecVal(1,s)
v2 = BitVecVal(2,s)
v3 = BitVecVal(3,s)

a = BitVec('a',s)
b = BitVec('b',s)
c = BitVec('c',s)

solv = Solver()

solv.add(node(v1))
solv.add(node(v2))
solv.add(node(v3))

solv.add(edge(v1, v2))
solv.add(edge(v2, v3))
solv.add(Not(edge(v1, v3)))
solv.add(Not(edge(v3, v2)))
solv.add(Not(edge(v2, v1)))
solv.add(Not(edge(v3, v1)))

solv.add(Implies(
    path(a, b),
    Or(
        And([node(a), node(b), edge(a, b)]),
        Exists(
            [c],
            And([
            node(a),
            node(b),
            node(c),
            edge(a, c),
            path(c, b)            
            ])
        )
    )
))

solv.add(Implies(
    Or(
        And([node(a), node(b), edge(a, b)]),
        Exists(
            [c],
            And([
            node(a),
            node(b),
            node(c),
            edge(a, c),
            path(c, b)            
            ])
        )
    ),
    path(a, b)
))

solv.add(path(v2, v1))
if solv.check():
    print(solv.model())
else:
    print('unsat')